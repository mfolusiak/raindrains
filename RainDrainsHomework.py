import numpy as np

class Drain:
    """
    Single drain data.
    Stores indices of drains left and right point and reference to the original data
    """
    left_idx  = None
    right_idx = None
    data      = None

def get_point_coordinates(drainsXY):
    """
    Extract and sort start and end points of all drains.
    Returns:
    sorted list of all points and sorting order (idx_argsort)
    >>> get_point_coordinates([[1,  4,8],[2,  5,11]])[0]
    [4, 5, 8, 11]
    """
    pt_coor = []
    for i in range(len(drainsXY)):
        pt_coor.append(drainsXY[i][1])
        pt_coor.append(drainsXY[i][2])
    # sort pt_coor from left to right
    idx_argsort = np.argsort(pt_coor)
    pt_coor = np.array(pt_coor)
    pt_coor = pt_coor[idx_argsort]
    pt_coor=list(pt_coor)
    return pt_coor, idx_argsort

def build_dra(drainsXY):
    """
    Build list of Drain() and extract point coordinates from original drains
    >>> dra, pt_coor = build_dra([[1,4,8],[2,5,11]])
    >>> [(d.left_idx,d.right_idx) for d in dra]
    [(0, 2), (1, 3)]
    >>> pt_coor
    [4, 5, 8, 11]
    """
    pt_coor, idx_argsort = get_point_coordinates(drainsXY)
    
    # use sorting order to avoid iterations and searching
    dra = [Drain() for _ in range(len(drainsXY))]
    for j in range(len(idx_argsort)):
        i = int(idx_argsort[j]/2)
        if (idx_argsort[j]%2==0): dra[i].left_idx=j
        else: dra[i].right_idx=j
        dra[i].data = drainsXY[i]
    return dra, pt_coor

def get_rpoints_drains_mapping(dra):
    """
    Maps right(end) points from to index of the drain for all drains
    >>> dra, pt_coor = build_dra([[1,4,8],[2,5,11]])
    >>> get_rpoints_drains_mapping(dra)
    {2: [0], 3: [1]}
    """
    rpoints = dict(list())
    for i in range(len(dra)):
        idx=dra[i].right_idx
        # for the right endpoint of each drain store 
        # ids of drains that have right point at this point
        if idx in rpoints: rpoints[idx].append(i)
        else: rpoints[idx]=[i]
    return rpoints

def get_layers_projection(dra):
    """
    Create projection of all rain drains to each point and ranges inbetween points
    Parameters:
    dra (list(Drain()))  : list of drains
    Returns:
    Each element of the doctionary represents:
        * integer keys - one point in the domain
        * integer + 0.5 keys - range between two points in the domain
    For each key in the dictionary the value is set to the list of drains
    above each point or range in ascending order. The topmost in each list
    is exposed to the rain.
    
    In the example below, there are two drains:
        0  : is the left point of the lower drain
        0.5: is the range between previous and next points, only lower drain here
        1  : is the left point of the upper drain, lower and upper drains here
        1.5: is the range between previous and next points, lower and upper drains here
        2  : is the right point of the lower drain, only upper drain here
        2.5: is the range between previous and next points, only upper drain here
        3  : is the right point of the upper drain, only upper drain here
    
    >>> dra, pt_coor = build_dra([[1,4,8],[2,5,11]])
    >>> get_layers_projection(dra)
    {0: [0], 0.5: [0], 1: [0, 1], 1.5: [0, 1], 2: [0, 1], 2.5: [1], 3: [1]}
    """
    layers=dict()
    for j in range(len(dra)):
        for i in range(dra[j].left_idx,dra[j].right_idx+1):
            if i in layers: layers[i].append(j)
            else: layers[i]=[j]
            # ranges
            if i!=dra[j].right_idx: 
                if i+0.5 in layers:
                    layers[i+0.5].append(j)
                else:
                    if i!=dra[j].right_idx: layers[i+0.5]=[j]
    return layers

def get_drains_right_connectivity(rpoints,layers):
    """
    Given right end point location of each drain and layers
    representing projections of drains returns edges of directional
    graph of conectivity between drains.

    >>> get_drains_right_connectivity({2: [0], 3: [1]},{0: [0], 0.5: [0], 1: [0, 1], 1.5: [0, 1], 2: [0, 1], 2.5: [1], 3: [1]})
    {}
    >>> drains=[[1,  5,11],[2,  4,8]]
    >>> dra,pt_coor=build_dra(drains)
    >>> pt_coor
    [4, 5, 8, 11]
    >>> rpoints=get_rpoints_drains_mapping(dra)
    >>> rpoints
    {3: [0], 2: [1]}
    >>> layers=get_layers_projection(dra)
    >>> layers
    {1: [0, 1], 1.5: [0, 1], 2: [0, 1], 2.5: [0], 3: [0], 0: [1], 0.5: [1]}
    >>> get_drains_right_connectivity(rpoints,get_layers_projection(dra))
    {1: 0}
    """
    connectivity = dict()
    for i in rpoints:
        for r in rpoints[i]:
            idx=layers[i].index(r)
            if idx>0:
                connectivity[r] = layers[i][idx-1]
    return connectivity

def get_rain_rates(pt_coor,layers,N,rate_per_length=1.):
    """
    Calculate rainrates from the rain assuming
    uniform distribution of rain along the top edge
    rate_per_length units of water per unit of x
    >>> get_rain_rates([4, 5, 8, 11],{1: [0, 1], 1.5: [0, 1], 2: [0, 1], 2.5: [0], 3: [0], 0: [1], 0.5: [1]},2)
    [3.0, 4.0]
    """
    rainrates=[0]*N
    for i in range(0,len(pt_coor)-1):
        coorl=pt_coor[i]
        coorr=pt_coor[i+1]
        d=layers[i+0.5][-1]
        rainrates[d]=rainrates[d]+(coorr-coorl)*rate_per_length
    return rainrates

def get_final_rates(rainrates,connectivity):
    """
    Follow each node of connectivity graph from top to bottom drain to sum up
    massflow contributions
    >>> get_final_rates([0, 0, 1.0, 4, 1.5, 6, 4.0],{3: 1, 4: 2, 5: 4, 6: 2})
    [0, 4, 12.5, 4, 7.5, 6, 4.0]
    """
    massrates=rainrates.copy()
    for from_d in range(len(rainrates)-1,-1,-1):
        if from_d in connectivity:
            to_d=connectivity[from_d]
            massrates[to_d]+=massrates[from_d]
    return massrates
    
class RainDrainsSolver:
    """
    >>> drainsXY=[]
    >>> drainsXY.append([1,  4,8])
    >>> drainsXY.append([2,  5,11])
    >>> drainsXY.append([3,  14,19.5])
    >>> drainsXY.append([3.5,3,10])
    >>> drainsXY.append([4,  12,15])
    >>> drainsXY.append([5,  7,13])
    >>> drainsXY.append([5.5,14.5,18.5])
    >>> s = RainDrainsSolver(drainsXY)
    >>> s.solve()[1]
    [4.0, 6.0, 7.5, 4.0, 12.5, 4.0, 0]
    """

    _N=None
    _dra=None
    _pt_coor=None
    _rates=None
    _rate_per_length=None
    
    def solve(self):
        """
        solves the problem, returns rates
        """
        if not self._rates:
            rpoints = get_rpoints_drains_mapping(self._dra)
            # project the problem to x axis with points and ranges with layered drains listed for each point/range
            layers = get_layers_projection(self._dra)
            # get connectivity graph edges
            connectivity = get_drains_right_connectivity(rpoints,layers)
            # rain rates initialized for each drain exposed to the drain
            rainrates = get_rain_rates(self._pt_coor,layers,self._N,self._rate_per_length)
            # adding rate contributions from top to down drain
            self._rates = get_final_rates(rainrates,connectivity)
        return [ _d.data for _d in self._dra[::-1] ],self._rates[::-1]

    def plot(self):
        # plot
        import matplotlib.pyplot as plt
        if not self._rates: self.solve()
        for d,i in zip( [ _d.data for _d in self._dra[::-1] ],range(len(self._dra))[::-1]):
            plt.plot([d[1],d[2]],[d[0],d[0]],label=self._rates[i])
        plt.legend()

    def __init__(self,drainsXY,rate_per_length=1):
        """
        constructor of the problem solver

        Parameters:
        drainsXY ([[float,float,float]]): list of drains in height-ascending order in the following format [ height, left_coord, right_coord ]
        """
        drainsXY.sort()
        self._N=len(drainsXY)
        self._rate_per_length=rate_per_length
        self._dra, self._pt_coor = build_dra(drainsXY)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
    
    '''
    input section
    '''
    N=7
    rate_per_leghth=1
    drainsXY=[]
    drainsXY.append([1,  4,8])
    drainsXY.append([2,  5,11])
    drainsXY.append([3,  14,19.5])
    drainsXY.append([3.5,3,10])
    drainsXY.append([4,  12,15])
    drainsXY.append([5,  7,13])
    drainsXY.append([5.5,14.5,18.5])
    
    '''
    solve
    '''
    homework = RainDrainsSolver(drainsXY,rate_per_leghth)
    drains, rates=homework.solve()
    print("Drains and respective rates:")
    for d, r in list(zip(drains,rates)):
        print("  drain {} | {}".format(d,r))
    homework.plot()
